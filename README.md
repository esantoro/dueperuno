Due per uno
================

Due per uno è un'applicazione web che aiuta le persone che
vogliono prendere il treno a conoscere altre persone che
hanno necessità di fare lo stesso tragitto, al fine di
acquistare i biglietti del treno tramite Trenitalia attraverso
la vantaggiosa offerta "due per uno" pagando così un solo
biglietto, comprandone due, e viaggiando su buoni treni (freccia
bianca e freccia rossa)

## Tech Stack

| Necessità              | Tecnologia |
|------------------------|------------|
| Linguaggio server side | Go         |
| Database               | PostgreSQL |
| session-store          | Redis      |
| linguaggio client-side | Javascript |


## Progress plan

Implementare, nell'ordine:

1. Registrazione utenti
2. Annunci (creazione)
3. Annunci (ricerca)
4. Feedback

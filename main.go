package main ;

import (
	"os"
	"log"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/contrib/gzip"
	"gitlab.com/esantoro/dueperuno/controllers"
	"gitlab.com/esantoro/dueperuno/middlewares"
	"fmt"
)

var (
	HTTP_PORT string
	ROUTER *gin.Engine ;
	SESSION_STORE sessions.CookieStore
	SESSION_SECRET string
	DEBUG bool
)

func main() {
	log.Println("Starting up...")

	HTTP_PORT = os.Getenv("PORT")
	if len(HTTP_PORT) == 0 {
		log.Println("[main] $PORT was not set, defaulting to 5000")
		HTTP_PORT = "5000"
	}

	// DEBUG will be true if $DEBUG is set, no matter what its value is
	DEBUG = os.Getenv("DEBUG") != ""
	if DEBUG {
		log.Println("$DEBUG is set, debug mode ENABLED")
	} else {
		log.Println("$DEBUG unset, debug mode DISABLED")
	}

	SESSION_SECRET = os.Getenv("SESSION_SECRET")
	if len(HTTP_PORT) == 0 {
		log.Println("[main] $SESSION_SECRET was not set, defaulting.")
		SESSION_SECRET = "I SHOULD HAVE SET A DIFFERENT SESSION_SECRET"
	}

	SESSION_STORE := sessions.NewCookieStore([]byte(SESSION_SECRET))
	SESSION_MIDDLEWARE := sessions.Sessions("session", SESSION_STORE)

	if DEBUG {
		ROUTER = gin.Default()
	} else {
		ROUTER = gin.New()
		ROUTER.Use(gin.Recovery())
	}

	ROUTER.Use(gzip.Gzip(gzip.DefaultCompression))

	ROUTER.Use(SESSION_MIDDLEWARE)
	ROUTER.Use(middlewares.AuthMiddleware)

	ROUTER.Static("/static", "./static")
	ROUTER.LoadHTMLGlob("views/*.html")


	ROUTER.GET("/", controllers.RootController)
	ROUTER.GET("/search", controllers.SearchController)
	ROUTER.GET("/add", controllers.SearchController)
	ROUTER.GET("/feedback", controllers.SearchController)
	ROUTER.GET("/checks", controllers.ChecksController)

	ROUTER.GET("/login", controllers.LoginController)
	ROUTER.POST("/login", controllers.LoginController)
	ROUTER.POST("/logout", controllers.LogoutController)

	ROUTER.GET("/signup", controllers.SignUpController)
	ROUTER.POST("/signup", controllers.SignUpController)

	ROUTER.GET("/test", controllers.TestController)


	ROUTER.Run(fmt.Sprintf(":%s", HTTP_PORT))

}

package controllers ;

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func RootController(ctx *gin.Context) {
	// ctx.String(http.StatusOK, "This is  the root controller")
  ctx.HTML(http.StatusOK, "index.html", ctx.Keys) ;
}

package controllers ;

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"fmt"
	"log"
	"bytes"
	"golang.org/x/crypto/bcrypt"
	"gitlab.com/esantoro/dueperuno/models"
	"github.com/gin-gonic/contrib/sessions"
	"time"
)

/*

 This is a base controller. This file is supposed to be copied and renamead as
 "SomethingController.go" and the controller function to be renamed to
 "SomethingController" in order to quickstart the development of a new
 controller.

*/

func LoginController(ctx *gin.Context) {
	if (ctx.Request.Method == "POST") {
		ctx.Request.ParseForm()

		email := ctx.PostForm("email")
		password := ctx.PostForm("password")

		user, present := models.GetUserByEmail(email)
		if !present  {
			// TODO: set a flash message
			ctx.Redirect(http.StatusSeeOther, "/login")
		}

		password_attempt_bytes := bytes.NewBufferString(password).Bytes()
		password_hash_bytes := bytes.NewBufferString(user.Password).Bytes()

		login_err := bcrypt.CompareHashAndPassword(password_hash_bytes, password_attempt_bytes)

		if login_err != nil {
			log.Println(fmt.Sprintf("[LoginController] Login failure for user %q", user.Username))
			log.Println(login_err)
			ctx.Redirect(http.StatusSeeOther, "/login") // TODO: set a flash message
			return
		} else {
			log.Println(fmt.Sprintf("[LoginController] user '%s' logged in (%s)", user.Username, time.Now()))

			session := sessions.Default(ctx)
			session.Set("username", user.Username)
			session.Save()

			ctx.Redirect(http.StatusSeeOther, "/") // TODO: redirect to annunci
		}


	} else {
		ctx.HTML(http.StatusOK, "login.html", gin.H{}) ;
	}
}

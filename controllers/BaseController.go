package controllers ;

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

/*

 This is a base controller. This file is supposed to be copied and renamead as
 "SomethingController.go" and the controller function to be renamed to
 "SomethingController" in order to quickstart the development of a new
 controller.

*/

func EDITME_Controller(ctx *gin.Context) {
	ctx.String(http.StatusOK, "CHECKS_OK")
}

package controllers

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/esantoro/dueperuno/models"
	"golang.org/x/crypto/bcrypt"
	"net/http"
	"fmt"
	"bytes"
	"log"
)

/*

 This is a base controller. This file is supposed to be copied and renamead as
 "SomethingController.go" and the controller function to be renamed to
 "SomethingController" in order to quickstart the development of a new
 controller.

*/

func SignUpController(ctx *gin.Context) {

	if ctx.Request.Method == "GET" {
		ctx.HTML(http.StatusOK, "signup.html", gin.H{})
	}

	if ctx.Request.Method == "POST" {
		ctx.Request.ParseForm()

		password := ctx.PostForm("password")
		password_confirm := ctx.PostForm("password2")
		ctx.Request.PostForm.Del("password2")

		if password == "" || (password != password_confirm) {
			ctx.HTML(http.StatusOK,
				"signup.html",
				gin.H{"error_msg": "Le password non coincidono!"})
			return
		}

		var user models.User = models.User{}

		// derr being decode-error
		derr := models.DECODER.Decode(&user, ctx.Request.PostForm)
		if derr != nil {
			// set some flash message then redirect to /signup
			log.Println("[SignUpController] unable to decode user")
			log.Println(derr)
			log.Println(user)
			ctx.HTML(http.StatusOK,
				"signup.html",
				gin.H{"error_msg": "Check input"})
			return
		}

		password_bytes := bytes.NewBufferString(user.Password).Bytes()
		hash, err := bcrypt.GenerateFromPassword(password_bytes, 3)
		if err != nil {
			log.Println("Error hashing password:")
			log.Println(err)
		}
		user.PlainPassword = password;
		user.Password = string(hash)

		log.Println(fmt.Sprintf("About to save user '%s/%s' with password '%s'",
								user.Username, user.Email, password))
		models.DB.Create(&user)

		if models.DB.NewRecord(user) == true {
			// log all the errors!
			log.Println("Unable to save new user to DB -- Printing all of the errors from database layer:")
			all_errors := models.DB.GetErrors()
			for _, e := range all_errors {
				log.Println("------------------------------")
				log.Println(e)
				log.Println("------------------------------")
			}
			log.Println("End of errors from saving new user to DB")

			ctx.HTML(http.StatusServiceUnavailable,
				"signup.html",
				gin.H{"error_msg": "Oops, qualcosa è andato storto."})
		} else {
			ctx.Redirect(http.StatusSeeOther, "/")
		}
	}
}

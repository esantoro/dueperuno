package controllers ;

import (
	"github.com/gin-gonic/gin"
	// "gitlab.com/esantoro/dueperuno/models"
	"net/http"
)

/*
	Test controller is useful to test various things without
	editing working controllers.

	It's basically a scratchpat to test stuff.
*/

func TestController(ctx *gin.Context) {

	ctx.HTML(http.StatusOK, "test.html", ctx.Keys)

}

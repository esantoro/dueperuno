package controllers ;

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

/*
 SearchController: Cerca un annuncio
*/

func SearchController(ctx *gin.Context) {
	ctx.HTML(http.StatusOK, "search.html", gin.H{})
}

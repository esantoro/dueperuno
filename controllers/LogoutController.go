package controllers ;

import (
	"github.com/gin-gonic/gin"
	"net/http"

	// "gitlab.com/esantoro/dueperuno/models"
	"github.com/gin-gonic/contrib/sessions"
	"log"
	"fmt"
	"time"
)

/*

 This is a base controller. This file is supposed to be copied and renamead as
 "SomethingController.go" and the controller function to be renamed to
 "SomethingController" in order to quickstart the development of a new
 controller.

*/

func LogoutController(ctx *gin.Context) {
	if ctx.Request.Method != "POST" {
		ctx.String(http.StatusMethodNotAllowed,
			"I don't know what youre doing but this is not the way you should do it.")
	}

	session := sessions.Default(ctx)
	u := session.Get("username")
	if u.(string) != "" {
		log.Println(fmt.Sprintf("[LogoutController] user '%s' logged out (%s)", u, time.Now()))
	}

	session.Clear()
	session.Save()

	ctx.Redirect(http.StatusSeeOther, "/")

}

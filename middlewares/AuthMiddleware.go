package middlewares ;

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/esantoro/dueperuno/models"
	"github.com/gin-gonic/contrib/sessions"
	"log"
)

/* This is the AuthMiddleware. Its job is to check whether there is an user session
	and if so, set the user id accordingly for the rest of the controller execution
	chain.

	Also, it should prevent unlogged users from accessing registered-users-only areas
	of the website.

*/
func AuthMiddleware(ctx *gin.Context) {
	session := sessions.Default(ctx)
	username := session.Get("username")

	if username != nil && username.(string) != "" {
		user, present := models.GetUserByUsername(username.(string))

		if ! present {
			log.Println("[WARNING/AuthMiddleware] I pulled an unexisting user out of a session. This does not make sense. ")
		} else {
			// everything is fine
			ctx.Set("current_user", user)
			ctx.Set("logged_in", true)
		}
	}

	ctx.Next()
}

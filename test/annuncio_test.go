package test ;

import (
	"testing"
	"gitlab.com/esantoro/dueperuno/models"
)


func TestSaveAnnuncio(t *testing.T) {

	u := models.User{
		Username: "qualcosa",
		Email: "qualcosa@qualcuno.tld",
		Password: "password",
		PlainPassword : "password",
	}
	u.Save()
	
	id := u.ID
	na := models.Annuncio{
		From:   "Milano",
		To:     "Lecce",
		UserID:  u.ID,
		Body:   "TESTING_BODY",
	}
	
	id, err := na.Save() ;
	if err != nil {
		t.Log(err)
		t.Fatal("Unable to save annuncio to DB")
	}
	
	if id != na.ID {
		t.Fatalf("Got id '%d', was expecting '%d'", id, na.ID)
	}
}

func TestAllAnnunci(t *testing.T) {

	u, _ := models.GetUserByUsername("qualcosa")

	na := models.Annuncio{
		From:   "Milano",
		To:     "Lecce",
		UserID:  u.ID,
		Body:   "TESTING_BODY",
	}
	na.Save()

	na2 := models.Annuncio{
		From:   "Milano",
		To:     "Lecce",
		UserID:  u.ID,
		Body:   "TESTING_BODY",
	}
	na2.Save()

	_, tot := u.GetAnnunci() ;
	if tot != 3 {
		t.Fatalf("Got '%d' annunci, was expecting 3")
	}

	
	
}

func TestLastAnnuncio(t *testing.T) {
	u, _ := models.GetUserByUsername("qualcosa")
	
	last, has_any := u.LastAnnuncio()

	if has_any {
		if last.Body != "TESTING_BODY" {
			t.Fatal("Last annuncio Body field does not match.") ;
		}
	} else {
		t.Fatal("User was supposed to have at least one Annuncio.") ;
	}
}

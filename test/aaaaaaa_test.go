package test ;

import (
	"gitlab.com/esantoro/dueperuno/utils"
	"gitlab.com/esantoro/dueperuno/models"
	"github.com/jinzhu/gorm"
	"testing"
	"os"
)

var (
	db gorm.DB
)


func TestMain(m *testing.M) {
	utils.InitSchema()

	db = models.DB
	os.Exit(m.Run())
}

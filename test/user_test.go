package test ;

import (
	"testing"
	"gitlab.com/esantoro/dueperuno/models"
)

func TestCreateUser(t *testing.T) {
	user := models.User{
		Username:"testing_user",
		Email: "test@somewhere.tld",
		Password:"$2a$10$y0pvyBoMjDhuvKsUkDL6nOUG3W3abpKZIdhJvdMtDTpxIYS06SG5W",
		PlainPassword:"qualcosa",
	}

	db.Create(&user) ;

	if db.NewRecord(user) {
		t.Fatal("unable to save new user to db")
	}
}

func TestSaveUser(t *testing.T) {
	user := models.User{
		Username:"save_testing_user",
		Email: "save_testing_user@qualcosa.tld",
		Password:"$2a$10$y0pvyBoMjDhuvKsUkDL6nOUG3W3abpKZIdhJvdMtDTpxIYS06SG5W",
		PlainPassword:"qualcosa",
	}

	id, err := user.Save()
	if err != nil {
		t.Fatal("Unable to save user.")
	}

	if id != user.ID {
		t.Log("returned id: ", id)
		t.Log("struct ID field: ", user.ID)
		t.Fatal("Returned id is different from primary key.") ;
	}

	if db.NewRecord(user) {
		t.Fatal("User saving was okay, but primary keys is not set.") 
	}
}

func TestGetByUsername(t *testing.T) {
	username := "save_testing_user"
	u, present := models.GetUserByUsername(username)

	if ! present {
		t.Fatal("Unable to pull existing user via GetUserByUsername") 
	}

	if u.Username != username {
		t.Log("Expected username: ", username)
		t.Log("Received username: ", u.Username)

		t.Fatal("Wrong user pulled out of DB")
	}
}

func TestGetByEmail(t *testing.T) {
	email := "save_testing_user@qualcosa.tld"
	u, present := models.GetUserByEmail(email)

	if ! present {
		t.Fatal("Unable to pull existing user via GetUserByEmail") 
	}

	if u.Email != email {
		t.Log("Expected email: ", email)
		t.Log("Received email: ", u.Email)

		t.Fatal("Wrong user pulled out of DB")
	}
}

func TestGetUserByID(t *testing.T) {
	var id uint = 1 ;
	u, present := models.GetUserById(id)

	if ! present {
		t.Fatal("Unable to pull existing user via GetUserByID") 
	}

	if u.ID != id {
		t.Log("Expected id: ", id)
		t.Log("Received id: ", u.ID)

		t.Fatal("Wrong user pulled out of DB")
	}
}


func TestGetUser(t *testing.T) {
	var user models.User ;
	db.Where("username = ?", "testing_user").Find(&user);

	if db.NewRecord(user) {
		t.Fatal("db.Where did not pull the user out of the DB correctly.")
	}
	
}


func TestRemoveUser(t *testing.T) {
	var user models.User ;
	db.Where("username = ?", "testing_user").Find(&user);

	if db.NewRecord(user) {
		t.Fatal("user primary key was not supposed to be blank!!")
	}

	db.Delete(&user) ;
	
	var blankUser = models.User{}
	
	db.Where("username = ?", "testing_user").Find(&blankUser) ;
	
	t.Log("removed user id: ", blankUser.ID)
	
	if blankUser.ID != 0 {
		t.Fatal("query returned an user with a non-null primary key.")
	}
}

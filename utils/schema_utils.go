package utils ;

import (
	"gitlab.com/esantoro/dueperuno/models"
	"log"
	"encoding/csv"
	"os"
	"fmt"
	"strconv"
)


func InitSchema() {
	log.Println("[create_schema] Registering model 'User'")
	models.DB.AutoMigrate(&models.User{})

	log.Println("[create_schema] Registering model 'Annuncio'")
	models.DB.AutoMigrate(&models.Annuncio{})

	log.Println("[create_schema] Registering model 'Stazione'")
	models.DB.AutoMigrate(&models.Stazione{})

	fmt.Println("About to import train stations into the DB") ;

	fd, _ := os.Open("../data/stazioni_clean.csv")

	rdr := csv.NewReader(fd) ;
	
	r, _ := rdr.Read() 
	fmt.Println(r)

	total := 0
	var e error
	var record []string
	var staz models.Stazione

	for {
		record, e = rdr.Read()
		if e != nil {
			break ;
		} else {
			staz = RecordToStazione(record);
			models.DB.Create(&staz)
			// fmt.Println("Imported station: ", staz)
			total += 1
		}
	}
	
	fmt.Println(fmt.Sprintf("Inserted %d stazioni", total))
}

func RecordToStazione(record []string) models.Stazione {
	// f, err := strconv.ParseFloat("3.1415", 64)
	// [Longitudine Provincia Comune Nome Latitudine]

	lon, _ := strconv.ParseFloat(record[0], 32)
	lat, _ := strconv.ParseFloat(record[4], 32)

	stazione := models.Stazione{
		Longitudine: float32(lon),
		Latitudine: float32(lat),
		Provincia: record[1],
		Comune: record[2],
		Nome: record[3],
	}

	return stazione
}

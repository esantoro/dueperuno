package models

import (
	"github.com/jinzhu/gorm"
	_ "github.com/lib/pq"
	"log"
	"os"
	"github.com/gorilla/schema"
	// "database/sql"
)

var (
    DEBUG	 bool
    DATABASE_URL string
    DB_TYPE      string
    DB  	 gorm.DB
    err 	 error
		DECODER 		*schema.Decoder
)

func init() {
	log.Println("[models/init] Beginning models initialization")

	DEBUG = os.Getenv("DEBUG") != ""

	DECODER = schema.NewDecoder()


	/* Read variables from environment variables: */
	DATABASE_URL = os.Getenv("DATABASE_URL")
	if len(DATABASE_URL) == 0 {
		log.Fatal("[models/init] $DATABASE_URL was not set, exiting.")
	}

	DB_TYPE = os.Getenv("DB_TYPE")
	if len(DB_TYPE) == 0 {
		log.Fatal("[models/init] $DB_TYPE was not set, exiting.")
	} else if DB_TYPE == "postgresql" {
		log.Println("[models/init] $DB_TYPE was set to 'postgresql', fixing to 'postgres'")
		DB_TYPE = "postgres"
	}

	/* Initialize the database connections here. */
	DB, err = gorm.Open(DB_TYPE, DATABASE_URL)
	if err != nil {
		log.Println("[models/init] Failed to instantiate database connection. Error:")
		log.Fatal(err)
	}
}

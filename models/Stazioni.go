package models ;

import (
	"github.com/jinzhu/gorm"
	"fmt"
)

type Stazione struct {
	gorm.Model
	Latitudine   float32
	Longitudine  float32
	Provincia    string
	Comune       string
	Nome         string	
}

func (self Stazione) TableName() string {
	return "stazioni"
}

func (self Stazione) String() string {	
	return fmt.Sprintf("%s - %s (%s)",self.Comune, self.Nome, self.Provincia)
}

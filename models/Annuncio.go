package models;

import (
	"github.com/jinzhu/gorm"
	"errors"
)

type Annuncio struct {
	gorm.Model
	From          string
	To            string
	Body          string  `sql:"type:text"`
	User          User
	UserID        uint
}

func (this Annuncio) TableName() string {
	return "annunci" ;
}

func (this *Annuncio) Save() (uint, error) {
	if ! DB.NewRecord(this) {
		return 0, errors.New("Primary key is non-blank (not a new instance)")
	} else {	
		DB.Create(this)
		
		return this.ID, nil
	}
}

func (self Annuncio) Validate() error {
	/* TODO: implement validation
	Validation strategy:
		- There should not be an existing user with the same username
		- There should not be an existing user with the same email address

	Also: update the return value to be map[string]array[]string, which is
	a map from string to an array of string, to be used in the following way:

	var messages map[string]array[]string = make(map[string][]string)
	messages["errors"] = make([]string)
	append(message["errors"], "Username 'xyz' already taken")
	append(message["errors"], "Another error message")

	messages["warnings"] = make([]string)
	append(messages["warnings"], "the password is too weak")

	return (messages, string)
	*/

	return nil;
}



func GetAnnuncioById(id int) *Annuncio {
	var annuncio Annuncio
	DB.First(&annuncio, id)

	return &annuncio
}

func (self Annuncio) String() string {
	return self.Body
}


package models;

import (
	"errors"
	"fmt"
	"github.com/jinzhu/gorm"
)

type User struct {
	gorm.Model
	Username      string `schema:"username" sql:"unique"`
	Password      string `schema:"password"`
	PlainPassword string
	Email         string `schema:"email" sql:"unique"`

	Annunci          []Annuncio
}

func  (self User) GetAnnunci() ([]Annuncio, uint) {
	var annunci []Annuncio
	var a Annuncio = Annuncio{}

	DB.Model(&a).Where("user_id = ?", self.ID).Find(&annunci)

	return annunci, uint(len(annunci))
}

func (self User) LastAnnuncio() (*Annuncio, bool) {
	var annuncio Annuncio

	// DB.Model(annuncio).Where("user_id = ?", self.ID).First(annuncio)
	// DB.LogMode(true)

	DB.Model(&annuncio).Where("user_id = ?", self.ID).Last(&annuncio)

	// fmt.Println("[LastAnnuncio]: ", annuncio)

	if DB.NewRecord(annuncio) {
		return &annuncio, false
	} else {
		return &annuncio, true
	}
}

func (self *User) Validate() error {
	/* TODO: implement validation
	Validation strategy:
		- There should not be an existing user with the same username
		- There should not be an existing user with the same email address

	Also: update the return value to be map[string]array[]string, which is
	a map from string to an array of string, to be used in the following way:

	var messages map[string]array[]string = make(map[string][]string)
	messages["errors"] = make([]string)
	append(message["errors"], "Username 'xyz' already taken")
	append(message["errors"], "Another error message")

	messages["warnings"] = make([]string)
	append(messages["warnings"], "the password is too weak")

	return (messages, string)
	*/

	return nil;
}



func GetUserById(id uint) (*User, bool) {
	var user User
	DB.First(&user, id)

	if DB.NewRecord(&user) {
		return &user, false
	} else {
		return &user, true
	}

}

func GetUserByUsername(username string) (*User, bool) {
	var user User
	DB.Where("username = ?", username).First(&user)
	// log.Println("[GetUserByUsername] pulled user '", user.Username, "' out of db")

	if user.Username != "" {
		return &user, true
	} else {
		return &User{}, false
	}
}

func GetUserByEmail(email string) (*User, bool) {
	var user User
	DB.Where("email = ?", email).First(&user)
	// log.Println(fmt.Sprintf("[GetUserByEmail] pulled user %q out of db", user.Username))

	if user.Username != "" {
		return &user, true
	} else {
		return &User{}, false
	}
}

func (u User) String() string {
	return fmt.Sprintf("User '%s' - email '%s'", u.Username, u.Email)
}

func (this *User) Save() (uint, error) {
	if ! DB.NewRecord(this) {
		return 0, errors.New("Primary key is non-blank: this is not a new instance")
	}
	
	DB.Create(this)

	return this.ID, nil ;
}

func (u User) Update() error {
	// update the user
	return errors.New("wat") ;
}
